Sophie Hou(lh6032):
Project 3 / Network

1. Goal:  implement a reliable transfer protocol (RDT) with UDP
For this project, I chose Selective Repeating protocol to implement RDT with four tools:
1).Checksum: to detect bit errors.
2).Acks: to record received packets and track unreceived ones.
3).Timers: to interrupt the sender after a given amount of timer has expired.
4).Sequence Number: to identify each packet.



2. Test Requirements: (for Test result: see 6.)
1). sending program: send a stream (or an infinitely long file) to a destination program
2). destination program: write out the file
3). packets sent/receive can be captured using a packet capturing tool



3. Implementation Class:
1).For Client side: Client.java, ReliableTransferProtocolClient.java, Packet.java
-Client.java: Driver class.
-ReliableTransferProtocolClient.java:
 * Run Reliable Transfer Protocol, transmit the packets (stored by Packet.java) with limit of  window size first, then
 * wait for ack from server. If get ack as expected (in order), then slide down window and transmit the next packet;
 * If acked number is out of order then window don't move. After timeout, resend lost packets.
-Packet.java: Combine header with file content data.

2).For Server side: Server.java, ReliableTransferProtocolServer.java
-Server.java: Driver class.
-ReliableTransferProtocolServer.java:
 * Run Reliable Transfer Protocol, receive the packets with limit of window size first, if packets are received in order,
 * then slide down window and send ack back to client, deliver packets to write out.
 * If packets are out of order, then buffer them in lowerLayer_Server first, then wait for the expected packet from
 * the client. When lowerLayer_Server is full, deliver packets to write out.


4. Implementation Details:
1).Client assigns a sequence number to each packet, it will be used by server to verify received packets. After server
received any packets, if they are in window boundary, then they will be buffered and the server send message with Ack
number to the client to tell which packets are received successfully. Based on such information, client can judge which
packets are lost or corrupted. Then it will trigger resend mechanism. For both sides of client and server, if they
received any packets in order as expected, then window will slide down to next packet. By using selective repeating can
allow packets arrive in order.

2).Used 4 bytes to store checksum, which can improve the accuracy than 2 bytes. In addition, checksum can detect bit
errors. The sender computes a checksum first and then attached to the header of packet. For Server, after received
the packet, it will extract the computed checksum from the header and then compute another checksum with the left real
data. By comparing with them, we can detect whether the packet is corrupted or not.

3).For avoiding lost packets, the sender keep a timer for each round packets sent. if an acknowledgement is not received
 within an estimated transit time. then it will be retransmitted.



5. Particular Explanation:
1).Transport Protocol: packets are tunneled within UDP for simulation.

2).Commandline Parameters:
--Client.java: two parameters (hostname and file path to be read).
--Server.java: two parameters (hostname and file path to be written).

3).Packet Format:
In my design, each packet holds a header with size of 5 bytes as following:

|-1 byte (Sequence Number)-|-4 bytes(Checksum)-|

Sequence Number: For simplify checking the result, sequence number ranges [0-255], after 255, it will restarts at 0 and
                 increments by 1. It applies for each round of 255. In my output, it will display the real packet number
                 (the first number) and computer readable number (the second one) which is inside'()', Output example:
                 "Sending packet: 256  (0)", "Sending packet: 8295 (103) "

4).Window Format:
My default window size is 4, it slides through each packet from 0 to the last packet. Output example:
                 "window boundary :[0, 4]", "window boundary :[158, 162]"

5).Close Socket: After 50 seconds, it will disconnect automatically.



6.Testing result:
1).It can transmit all kinds of file successfully, such as .PNG, .pdf, .doc.
2).It can be captured by WireShark: (Details can see attached picture in folder), example show as below:

No.	Time	Source	Destination	Protocol	Length	    Info
16542	25.361488	127.0.0.1	127.0.0.1	UDP	1056	8002 → 8001 Len=1024
No.	Time	Source	Destination	Protocol	Length	    Info
16541	25.361168	127.0.0.1	127.0.0.1	UDP	33	    8001 → 8002 Len=1

capture steps:
i). Mac os commandline: ping 127.0.0.1
ii). Open WireShark : Loopback
iii). Run Server.java, Client.java
iv.) Check WireShark: UDP protocol



7.Conclusion:
This protocol is much more efficient than TCP/IP, because it used UDP to simulate which doesn't need 3-way handshake.
Moreover, it used checksum, acks, sequence numbering and timer to assure the network reliability, so packets can arrive
in order.