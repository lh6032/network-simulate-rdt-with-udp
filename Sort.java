//package P3;
/**
 * Sort lowerLayer_Server() list in ReliableTransferProtocolServer.java
 *
 * Author: Sophie Hou(lh6032)
 */

import java.util.Comparator;

public class Sort implements Comparator<byte[]> {

    @Override
    public int compare(byte[] o1, byte[] o2) {
        return o1[0] - o2[0];
    }
}