//package P3;


/**
 * Uses UDP to simulate a reliable transfer protocol (selective repeating) including  checksum，ACKs, timers,
 * and sequence number. Call ReliableTransferProtocolClient.java to do the task.
 * @author  Sophie Hou (lh6032)
 */
import java.io.*;

/**
 * Driver class.
 */
public class Client {

    static String host = "";
    static String fileName = "";
    static int sourcePort = 8002;
    static int destPort = 8001;


    /**
     * Records runtime behavior and get arguments from commandline, i.e. port number,
     * server hostname. And then call requestFirst() and receiveData().
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        long startTime;
        long endTime;
        startTime = System.currentTimeMillis();

        if(args.length > 0){
            host = args[0];   // get host
            fileName = args[1];    // get file name to be read
        }else{
            System.out.println("Please input 'IP address' and 'file path' to be read from Commandline.");
            System.out.println("By default, 'IP address' = 'localhost.");
            System.out.println("By default, 'fileName' is just for testing', please input another one.");
            host = "localhost";
            fileName = "/Users/sophiehou/Desktop/NetWork-hw/Network-P3/test.pdf"; // for test
            sourcePort = 8002;
            destPort = 8001;

        }

        ReliableTransferProtocolClient protocol = new ReliableTransferProtocolClient(host, fileName, sourcePort, destPort);

        protocol.clientDoTheWork();

        endTime = System.currentTimeMillis();
        System.out.println("time : " + (endTime - startTime));

    }

}
