//package P3;

/**
 * From server side:  run Reliable Transfer Protocol, receive the packets with limit of window size first, if
 * packets are received in order, then slide down window and send ack back to client, deliver packets to write out.
 * If packets are out of order, then buffer them in lowerLayer_Server first, then wait for the expected packet from
 * the client. When lowerLayer_Server is full, deliver packets to write out.
 *
 * Author: Sophie Hou(lh6032)
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.Adler32;
import java.util.zip.Checksum;


public class ReliableTransferProtocolServer {
    FileOutputStream outputStream = null;
    File file;
    DatagramSocket serverSocket;
    DatagramPacket receivePacket;
    InetAddress ip;

    int sourcePort;
    int destPort;
    String fileName = "";
    List<byte[]> lowerLayer_Server = new LinkedList<byte[]>();  // buffer packets within window size

    final int WINDOW_SIZE = 4;
    final int PAYLOAD = 1024;
    final int HEADER_SIZE = 5; // 1 byte for sequence number, 4 bytes for checksum
    final int MAX_PAYLOAD = PAYLOAD - HEADER_SIZE; // maximum payload


    /**
     * Constructor
     * @params ip, fileName, sourcePort, destPort
     */
    public ReliableTransferProtocolServer(String ip, String fileName, int sourcePort, int destPort)
            throws UnknownHostException {
        this.ip = InetAddress.getByName(ip);
        this.fileName = fileName;
        this.sourcePort = sourcePort;
        this.destPort = destPort;
        file = new File(fileName); // create file path
    }


    public void serverDoTheWork() throws IOException{
        receiveFile();
    }

    /** Implements reliable transfer protocol.
     * After received packets, Calls bytesToLong() and setCheckSum() to compute checksum and compare with the attached
     * checksum in header to check the packet is corrupted or not.
     * Calls sendAck() to client.
     * Calls compare() to sort lowerLayer_Server.
     * Calls sendToUpperLayer() to deliver verified packets and write out file.
     */
    public void receiveFile() throws IOException {

        //server starts receiving packets
        serverSocket = new DatagramSocket(sourcePort);
        System.out.println("Server Listening on port: " + serverSocket.getLocalPort());

        serverSocket.setSoTimeout(1000); // set timer

        int rcv_base = 0;  // window starts
        int windowBoundary = WINDOW_SIZE;  // window ends

        long start = new Date().getTime();// network connection starts timing

        for (; ;) {
            try {
                byte[] receiveData = new byte[PAYLOAD]; //1024
                receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);
                byte[] buffer = receivePacket.getData(); // obtain received data

                // get received sequence number
                byte[] rcved_SeqNum = new byte[1];
                rcved_SeqNum[0] = buffer[0];
                int rcved_SeqNum_int =  rcved_SeqNum[0] < 0? (rcved_SeqNum[0] & 0xFF) : rcved_SeqNum[0];

                // get checksum from header
                byte[] temp = Arrays.copyOfRange(buffer, 1, HEADER_SIZE);
                long rcved_Checksum = bytesToLong(temp);
                System.out.print("Expected checksum = " + rcved_Checksum);

                // compute checksum based on transmitted data except header
                long sender_Checksum = setCheckSum(HEADER_SIZE, buffer, MAX_PAYLOAD);
                System.out.println(" |  Received checksum : " + sender_Checksum);

                // chceck two checksum results
                if (rcved_Checksum == sender_Checksum) {
                    System.out.println("window boundary :[ " + rcv_base + ", " + windowBoundary + "]");
                    System.out.println("received packet #: " + rcved_SeqNum_int);

                    if (rcv_base <= rcved_SeqNum_int && rcved_SeqNum_int < windowBoundary) {
                        lowerLayer_Server.add(buffer);   // packets within window boundary buffered first
                        sendAck(rcved_SeqNum);         // send ack back to client

                        for (int i = 0; i < lowerLayer_Server.size(); i++) {
                            // sort list to make the packet with smallest sequence number always stands at head of list
                            lowerLayer_Server.sort(new Sort());

                            // slide window
                            if (rcved_SeqNum_int == rcv_base) {
                                sendToUpperLayer(lowerLayer_Server.get(0));  // deliver packets to write out on file
                                lowerLayer_Server.remove(0);
                                // rcv_base and windowBoundary: if exceeds 255, restarts from 0
                                rcv_base = rcv_base < 255? rcv_base + 1 : rcv_base - 255;
                                windowBoundary = windowBoundary < (255 + WINDOW_SIZE)?
                                        windowBoundary + 1 : windowBoundary - 255;
                            }
                        }
                    }
                    if ((rcv_base - WINDOW_SIZE) < rcved_SeqNum_int && rcved_SeqNum_int < (rcv_base - 1)) {
                        sendAck(rcved_SeqNum);   // previous packets just send ack
                    }
                }

                long offLine = new Date().getTime();
                if (offLine - start > 50 * 1000) {   // stop connection
                    System.out.println("Exceeds 50 seconds, Stop Connection");
                    break;
                }
            }catch(SocketTimeoutException e){
            //System.out.println("Server time out.");
            }
        }
        serverSocket.close();  // close socket
    }

    /**
     * Sends ack back to client.
     * @param rcved_SeqNum
     */
    public void sendAck(byte[] rcved_SeqNum) throws IOException {
        receivePacket = new DatagramPacket(rcved_SeqNum, rcved_SeqNum.length, ip, destPort); // send ack back
        serverSocket.send(receivePacket);
        System.out.println("sending Ack " + (rcved_SeqNum[0] & 0xff));
    }

    /**
     * Deliver packets to write out on file.
     * @param buffer
     */
    public void sendToUpperLayer(byte[] buffer) throws IOException {
        outputStream = new FileOutputStream(file, true);
        outputStream.write(buffer, HEADER_SIZE, buffer.length - HEADER_SIZE);  // write out

        outputStream.flush();
        outputStream.close();
    }

    /**
     *  Compute checksum for the packet.
     */
    public final long setCheckSum(int offset, byte[] pkt, int pktLength) {
        Checksum checksum = new Adler32();
        checksum.update(pkt, offset, pktLength);
        return checksum.getValue();
    }

    /**
     * Convert bytes into long, to get the checksum.
     * @param temp
     */
    public long bytesToLong(byte[] temp) {
        long checksum = 0;
        for (int ix = 0; ix < HEADER_SIZE-1; ++ix) {
            checksum <<= 8;
            checksum |= (temp[ix] & 0xff);
        }
        return checksum;
    }

}
