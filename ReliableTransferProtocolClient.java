//package P3;


/**
 * From client side:  run Reliable Transfer Protocol, transmit the packets (stored by Packet.java) with limit of
 * window size first, then wait for ack from server. If get ack as expected (in order), then slide down window and
 * transmit the next packet; If acked number is out of order then window don't move. After timeout, resend lost packets.
 *
 * Author: Sophie Hou(lh6032)
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.Adler32;
import java.util.zip.Checksum;


public class ReliableTransferProtocolClient {
    FileInputStream fileInputStream = null;

    DatagramSocket cSocket;
    DatagramPacket datagramPacket;
    InetAddress ip;

    int sourcePort;
    int destPort;
    int size_segments = 0; // total packets
    String fileName = "";
    Packet[] packet;
    List<Packet> toBeResentBuffer_Client = new LinkedList<Packet>();

    final int WINDOW_SIZE = 4;
    final int PAYLOAD = 1024;
    final int HEADER_SIZE = 5; // 1 byte for sequence number, 4 bytes for checksum
    final int MAX_PAYLOAD = PAYLOAD - HEADER_SIZE; // maximum payload

    /**
     * Constructor
     * @params ip, fileName, sourcePort, destPort
     */
    public ReliableTransferProtocolClient(String ip, String fileName, int sourcePort, int destPort)
            throws UnknownHostException {
        this.ip = InetAddress.getByName(ip);
        this.fileName = fileName;
        this.sourcePort = sourcePort;
        this.destPort = destPort;
    }

    /**
     * Call sendFile() to do the task.
     * @throws IOException
     */
    public void clientDoTheWork() throws IOException {
        sendFile();
    }


    /** Implements reliable transfer protocol.
     * Before transmitting, pass sequence number in to be sent packet.
     * And Calls setCheckSum() to compute checksum, and pass it in to be sent packet.
     * After transmitted, Calls receiveAck() to check acked packet number.
     */
    public void sendFile() throws IOException {
        byte[] sendBuffer = new byte[MAX_PAYLOAD]; // 1021

        // client starts to send file to server
        cSocket = new DatagramSocket(sourcePort);

        File file = new File(fileName);
        // split file into packets
        long lOriginalFileSize = file.length();   // get total length of file
        System.out.println("file total length: " + lOriginalFileSize);

        // get number of packets
        size_segments = lOriginalFileSize % MAX_PAYLOAD == 0 ?
                (int) (lOriginalFileSize / MAX_PAYLOAD) : (int) (lOriginalFileSize / MAX_PAYLOAD)+1;
        System.out.println("File split into " + size_segments + " packets.");
        // size of last segment
        long lRemainderSize = lOriginalFileSize % MAX_PAYLOAD == 0 ? 0: lOriginalFileSize % (size_segments-1);

        int sender_base = 0; // window starts
        int windowBoundary = WINDOW_SIZE; // window ends
        int indexPacket = 0;

        long offset = 0;
        int receivedAck = 0;

        packet = new Packet[size_segments];

        long start = new Date().getTime();// network connection starts timing

        try {
            fileInputStream = new FileInputStream(file);   // read file
        } catch (FileNotFoundException e) {
            System.out.println("File not found, check again.");
        }

        byte[] temp = fileInputStream.readAllBytes();

        for (;;) {

            while(toBeResentBuffer_Client.size() < WINDOW_SIZE && indexPacket < size_segments )  {

                sendBuffer = Arrays.copyOfRange(temp, (int)offset, (MAX_PAYLOAD+(int)offset)); // get data of each packet

                long checksum = setCheckSum(0, sendBuffer, sendBuffer.length);  // get checksum
                // store : sequence number(1st byte),  checksum (2nd-5th byte)
                packet[indexPacket] = new Packet(indexPacket, checksum);

                for (int i = HEADER_SIZE, index = 0; i < packet[indexPacket].sendData.length; i++) {
                    packet[indexPacket].sendData[i] = sendBuffer[index++];       // add data to every packet
                }

                toBeResentBuffer_Client.add(packet[indexPacket]);  // add packets for resent
                System.out.println("\"toBeResentBuffer_Client\" size = " + toBeResentBuffer_Client.size());

                if (windowBoundary != size_segments) {
                    datagramPacket = new DatagramPacket
                            (packet[indexPacket].sendData, packet[indexPacket].sendData.length, ip, destPort);
                } else {
                    // send the last segment
                    byte[] theLastSegment = Arrays.copyOf(sendBuffer, (int) lRemainderSize);
                    datagramPacket = new DatagramPacket(theLastSegment, theLastSegment.length, ip, destPort);
                }
                cSocket.send(datagramPacket);  // send packet
                System.out.println("Sending packet: " + indexPacket + "  (" + (indexPacket & 0xff) + ")");
                indexPacket++;
                offset += MAX_PAYLOAD;  // skip transmitted data for next packet
            }

            cSocket.setSoTimeout(1000); // set timer

            //receive ack
            try {
                receivedAck = receiveAck();  // get acked number
                System.out.print("Expected Ack: " + sender_base);
                System.out.println("  |  Received Ack: " + receivedAck);

                if (sender_base == receivedAck) {
                    toBeResentBuffer_Client.remove(0); // remove the Acked packet from toBeResentBuffer
                    System.out.println("\"toBeResentBuffer_Client\" size = " + toBeResentBuffer_Client.size());
                    // slide window
                    // sender_base and windowBoundary: if exceeds 255, restarts from 0
                    sender_base = sender_base < 255 ? sender_base + 1 : sender_base - 255;
                    windowBoundary = windowBoundary < (255 + WINDOW_SIZE) ? windowBoundary + 1 : windowBoundary - 255;
                    System.out.println("window boundary :[ " + sender_base + ", " + windowBoundary + "]");
                }
            }catch(SocketTimeoutException e) {
                //System.out.println("Time Out.");
                if(toBeResentBuffer_Client.size() != 0) {
                    resendPacket();   // at time out, selective resend packet
                }
            }

            long offLine = new Date().getTime();
            if (offLine - start > 50* 1000) {   // after 50 seconds, stop connection
                System.out.println("Exceeds 50 seconds, Stop Connection");
                break;
            }
        }
        fileInputStream.close(); //close input stream
        cSocket.close(); // close socket
    }

    /**
     * Receive Ack from server.
     */
    public int receiveAck() throws IOException {
        byte[] ackBuffer = new byte[1];   // Ack holds one byte
        datagramPacket = new DatagramPacket(ackBuffer, ackBuffer.length);
        cSocket.receive(datagramPacket);
        return datagramPacket.getData()[0] < 0? (datagramPacket.getData()[0] & 0xFF) : datagramPacket.getData()[0];
    }

    /**
     * Resend packet.
     */
    public void resendPacket() throws IOException {
        datagramPacket = new DatagramPacket(
                toBeResentBuffer_Client.get(0).sendData, toBeResentBuffer_Client.get(0).sendData.length, ip, destPort);
        cSocket.send(datagramPacket);  // resend packet
        System.out.println("Selective Resending Packet " + toBeResentBuffer_Client.get(0).sequenceNum);
    }


    /**
     * Compute checksum for the packet.
     */
    public final long setCheckSum(int offset, byte[] pkt, int pktLength) {
        Checksum checksum = new Adler32();
        checksum.update(pkt, offset, pktLength);
        return checksum.getValue();
    }
}
