//package P3;

public class Packet {
    byte[] sendData;
    final int PAYLOAD = 1024;
    int sequenceNum = 0;
    long checksum = 0;

    public Packet(int sequenceNum, long checksum){
        sendData = new byte[PAYLOAD];
        sendData[0] = (byte) sequenceNum;

        // convert checksum long type to byte
        System.arraycopy(longTobytes(checksum), 0, sendData, 1, 4);

        this.sequenceNum = sequenceNum;
        this.checksum = checksum;
    }

    /**
     *  To transmit checksum through network, converts checksum long type to byte
     * @param checksum
     * @return
     */
    public byte[] longTobytes(long checksum){
        byte[] result = new byte[4];
        for (int i = 3; i >= 0; i--) {
            result[i] = (byte)(checksum & 0xFF);
            checksum >>= 8;
        }
        return result;
    }

}
