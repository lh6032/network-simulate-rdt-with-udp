//package P3;

/**
 * Uses UDP to simulate a reliable transfer protocol (selective repeating) including  checksum，ACKs, timers,
 * and sequence number. Call Server ReliableTransferProtocolServer.java to do the task.
 * @author  Sophie Hou (lh6032)
 */

import java.io.IOException;

/**
 * Driver class.
 */
class Server  {
    static String host = "";
    static String fileName = "";
    static int sourcePort = 8001;
    static int destPort = 8002;

    public static void main(String[] args) throws IOException {
        if(args.length > 0){
            host = args[0];    // get host
            fileName = args[1];  // get file path to be written
        }else{
            System.out.println("Please input 'IP address' and 'file path' to be written from Commandline.");
            System.out.println("By default, 'IP address' = 'localhost.");
            System.out.println("By default, 'fileName' is just for testing', please input another one.");
            host = "localhost";
            fileName = "/Users/sophiehou/Desktop/NetWork-hw/Network-P3/test0.pdf"; // for test
            sourcePort = 8001;
            destPort = 8002;

        }
        ReliableTransferProtocolServer protocol = new ReliableTransferProtocolServer(host, fileName, sourcePort, destPort);
        protocol.serverDoTheWork();
    }
}







